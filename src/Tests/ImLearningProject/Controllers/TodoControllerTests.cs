﻿using System;
using ImLearning.Models;
using Microsoft.Net.Http.Server;
using Xunit;

namespace Tests.ImLearningProject.Controllers
{
    public class TodoControllerTests
    {
        [Fact(DisplayName = "Should add")]
        public void Add()
        {
            var controller = new ImLearning.Controllers.TodoController();
            var todo = new Todo {Id = Guid.NewGuid(), Description = "Descr"};
            var res = controller.Add(todo);

            Assert.Equal(200, res.StatusCode);
        }
    }
}
