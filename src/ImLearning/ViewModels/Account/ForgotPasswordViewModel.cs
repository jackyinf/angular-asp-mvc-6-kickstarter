using System.ComponentModel.DataAnnotations;

namespace ImLearning.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
