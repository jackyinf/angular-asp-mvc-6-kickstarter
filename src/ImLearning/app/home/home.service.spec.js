describe("home.service.spec", function () {

    beforeEach(function() {
        //Ensure angular modules available
        module('app');
    });

    var $rootScope, HomeService;
    beforeEach(inject(["$rootScope",  "home.service", function (_$rootScope_, _HomeService_) {
        $rootScope = _$rootScope_;
        HomeService = _HomeService_;
    }]));

    it('should have HomeService service be defined', function () {
        expect(HomeService).toBeDefined();
    });

    it("should say hello", function () {
        HomeService.sayHello().then(function (response){
            expect(response).toBe("HELLO FROM HOME SERVICE");
        });
        $rootScope.$digest();        // resolves promises! :D
    });

    // http://jbavari.github.io/blog/2014/06/11/unit-testing-angularjs-services/
    //describe('Mocked HTTP Requests', function() {
    //
    //    var $httpBackend;
    //    var name = 'Josh Bavari';
    //    var email = 'jbavari@gmail.com';
    //
    //    beforeEach(inject(function($injector) {
    //        // Set up the mock http service responses
    //        $httpBackend = $injector.get('$httpBackend');
    //        $httpBackend.when('POST', 'http://raisemore.dev/api/v1/user/checkuser')
    //            .respond(200, {name: name, email: email, success: true});
    //    }));
    //
    //    afterEach(function() {
    //        $httpBackend.verifyNoOutstandingExpectation();
    //        $httpBackend.verifyNoOutstandingRequest();
    //    });
    //
    //
    //    it('should have sent a POST request to the checkuser API', function() {
    //        var result = Auth.checkUser(name, email, 1, '4408064001', null);
    //        $httpBackend.expectPOST('http://raisemore.dev/api/v1/user/checkuser');
    //        $httpBackend.flush();
    //    });
    //
    //});

});