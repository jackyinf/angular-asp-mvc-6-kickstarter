﻿const INIT = new WeakMap();
const SERVICE = new WeakMap();

class HomeController
{
    constructor($timeout, homeService) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'This came from Home controller';

        SERVICE.set(this, homeService);
        INIT.set(this, () => {
            console.log("INIT func called");
            //console.log(SERVICE.get(this).sayHello());
            SERVICE.get(this).sayHello().then(response => {
                console.log(response);
            });
        });

        this.activate();
    }

    activate() {
        console.log("Hello from home controller");
        INIT.get(this)();
    }
}

HomeController.$inject = ['home.service'];

export default HomeController;