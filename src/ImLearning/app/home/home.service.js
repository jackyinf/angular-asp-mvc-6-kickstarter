﻿const HTTP = new WeakMap();
const Q = new WeakMap();

class HomeService
{
    constructor($http, $q) {
        HTTP.set(this, $http);
        Q.set(this, $q);
    }

    sayHello() {
        return Q.get(this)(function(resolve) {
            resolve("HELLO FROM HOME SERVICE");
        });
    }

    static factory($http, $q) {
        return new HomeService($http, $q);
    }
}

HomeService.factory.$inject = ["$http", "$q"];

export default HomeService.factory;