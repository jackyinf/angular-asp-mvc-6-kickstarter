﻿import angular from "angular";
import HomeController from "./home.controller";
import HomeService from "./home.service";

const moduleName = "home.module";

angular.module(moduleName, [])
    .controller("home.controller", HomeController)
    .service("home.service", HomeService);

export default moduleName;