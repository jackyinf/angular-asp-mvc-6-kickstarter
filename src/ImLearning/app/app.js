﻿import "babelify/polyfill";
import angular from "angular";
import "angular-ui-router";
import angularResource from "angular-resource";
import "./templates";

import appInfo from "./app.info";
import egAppStatus from "./egAppStatus";

import homeModuleName from "./home/home.module";
import photoModuleName from "./photo/photo.module";
import config from "./app.config";

angular
    .module('app', [angularResource, 'ui.router', "templates", homeModuleName, photoModuleName])
    .config(config)
    .run(['$rootScope', '$state', function($rootScope, $state) {
        $rootScope.$on('$stateChangeStart', function(evt, to, params) {
            if (to.redirectTo) {
                evt.preventDefault();
                $state.go(to.redirectTo, params);
            }
        });
    }])
    .factory("appInfo", appInfo)
    .directive("egAppStatus", egAppStatus);