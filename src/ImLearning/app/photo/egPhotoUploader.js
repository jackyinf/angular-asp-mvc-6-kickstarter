﻿'use strict';
import "./../templates";

function egPhotoUploader(appInfo, photoManager) {

    var directive = {
        link: link,
        restrict: 'E',
        templateUrl: 'photo/egPhotoUploader.html',
        scope: true
    };
    return directive;

    function link(scope, element, attrs) {
        scope.hasFiles = false;
        scope.photos = [];            
        scope.upload = photoManager.upload;
        scope.appStatus = appInfo.status;
        scope.photoManagerStatus = photoManager.status;
    }
}

egPhotoUploader.$inject = ['appInfo','photoManager'];

export default egPhotoUploader;