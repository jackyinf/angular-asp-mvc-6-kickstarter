﻿import egFiles from "./egFiles";
import egPhotoUploader from "./egPhotoUploader";
import egUpload from "./egUpload";
import photoManager from "./photoManager";
import photoManagerClient from "./photoManagerClient";
import photos from "./photos";

const moduleName = "app.photo";

function configPhotos($stateProvider, $locationProvider) {
    //$locationProvider.html5Mode({
    //    enabled:true,
    //    requireBase: false
    //});
    $stateProvider
        //.state('app', {
        //    url: '/',
        //    views: {
        //        nav: { templateUrl: "nav.html" },
        //        content: { templateUrl: "app.html" }
        //    }
        //})
        //.state('home', {
        //    url: '/home',
        //    views: {
        //        nav: { templateUrl: "nav.html" },
        //        content: { templateUrl: "home/home.html" }
        //    }
        //})
        .state('photo', {
            url: '/photo',
            views: {
                nav: { templateUrl: "nav.html" },
                content: {
                    templateUrl: "photo/photos.html",
                    controller: photos,
                    controllerAs: "vm"
                }
            }
        });
        //.state("otherwise", {
        //    url: "*path",
        //    redirectTo: 'home'
        //    //views: {
        //    //    nav: {  },
        //    //    content: { templateUrl: "404.html" }
        //    //}
        //});
}

configPhotos.$inject = ['$stateProvider', '$locationProvider'];

angular.module(moduleName, [])
    .config(configPhotos)
    .directive('egFiles', egFiles)
    .directive('egPhotoUploader', egPhotoUploader)
    .directive('egUpload', egUpload)
    .factory('photoManager', photoManager)
    .factory('photoManagerClient', photoManagerClient)
    .controller('photos', photos);

export default moduleName;