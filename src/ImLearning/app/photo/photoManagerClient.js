﻿'use strict';

function photoManagerClient($resource) {
    return $resource("api/photo/:fileName",
            { id: "@fileName" },
            {
                'query': { method: 'GET' },
                'save': { method: 'POST', transformRequest: angular.identity, headers: { 'Content-Type': undefined } },
                'remove': { method: 'DELETE', url: 'api/photo/:fileName', params: { name: '@fileName' } }
            });
}

photoManagerClient.$inject = ['$resource'];

export default photoManagerClient;