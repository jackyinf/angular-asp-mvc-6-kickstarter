﻿function config($stateProvider, $locationProvider) {
    //$locationProvider.html5Mode({
    //    enabled:true,
    //    requireBase: false
    //});
    $stateProvider
        .state('app', {
            url: '/',
            views: {
                nav: { templateUrl: "nav.html" },
                content: { templateUrl: "app.html" }
            }
        })
        .state('home', {
            url: '/home',
            views: {
                nav: { templateUrl: "nav.html" },
                content: { templateUrl: "home/home.html" }
            }
        })
        //.state('photo', {
        //    url: '/photo',
        //    views: {
        //        nav: { templateUrl: "nav.html" },
        //        content: { templateUrl: "photo/photos.html" }
        //    },
        //    controller: 'photos'
        //})
        .state("otherwise", {
            url: "*path",
            redirectTo: 'home'
            //views: {
            //    nav: {  },
            //    content: { templateUrl: "404.html" }
            //}
        });
}

config.$inject = ['$stateProvider', '$locationProvider'];

export default config;