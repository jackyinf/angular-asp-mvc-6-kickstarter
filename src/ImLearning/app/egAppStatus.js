'use strict';
import "./templates";

function egAppStatus(appInfo) {
    var directive = {
        link: link,
        restrict: 'E',
        templateUrl: 'egAppStatus.html'
    };
    return directive;

    function link(scope, element, attrs) {
        scope.status = appInfo.status;
    }
}

egAppStatus.$inject = ['appInfo'];

export default egAppStatus;