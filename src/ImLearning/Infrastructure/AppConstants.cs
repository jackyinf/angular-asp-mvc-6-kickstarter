﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImLearning.Infrastructure
{
    internal static class AppConstants
    {
        /// <summary>
        /// Configuration section account.
        /// </summary>
        internal const string ConfigurationSectionAccount = "jackstorage1";
        
        /// <summary>
        /// Configuration section key.
        /// </summary>
        internal const string ConfigurationSectionKey = "z8ggDMPjcDCAeb6Uyg4EmPKh7vY+OtCBtO7RQ8KFNnpDKFH8nriScYFfHV6x0L44tz15rGNqiiI6McRHA0gvAQ==";

        /// <summary>
        /// Container where to upload files.
        /// </summary>
        internal const string ContainerName = "pictures";

        ///// <summary>
        ///// Number of bytes in a Kb.
        ///// </summary>
        //internal const int BytesPerKb = 1024;

        ///// <summary>
        ///// Name of session element where attributes of file to be uploaded are saved.
        ///// </summary>
        //internal const string FileAttributesSession = "FileClientAttributes";

        public static string GetConnectionStringForBlobStorage()
        {
            return "DefaultEndpointsProtocol=https;AccountName=" + ConfigurationSectionAccount + ";AccountKey=" + ConfigurationSectionKey;
        }
    }
}
