﻿module.exports = function (gulp, plugins, paths) {
    return {
        js: function () {
            /// <summary>
            /// Takes js application's enrty point and converts ES6 to ES5 using BROWSERIFY.
            /// Then, it adds comments to link source code - it shows original code in chrome inspector.
            /// </summary>

            var promises = [];
            paths.forEach(function (path) {
                var temp = plugins.browserify({ entries: path.appName + "/app.js", extensions: ['.js'], debug: true })
                    .transform(plugins.babelify)
                    .bundle()
                    .pipe(plugins.vinylSourceStream(path.appName + '.js'))
                    .pipe(plugins.vinylBuffer());
                var promise = temp.pipe(gulp.dest(path.buildFolder));
                promises.push(promise);
            });
            return plugins.q.all(promises);
        },
        jsDeploy: function () {
            /// <summary>
            /// Dublication of js function. Uglification is added.
            /// </summary>

            var promises = [];
            paths.forEach(function (path) {
                var promise = plugins.browserify({ entries: path.appName + "/app.js", extensions: ['.js'], debug: true })
                    .transform(plugins.babelify)
                    .bundle()
                    .pipe(plugins.vinylSourceStream(path.appName + '.js'))
                    .pipe(plugins.vinylBuffer())
                    .pipe(plugins.uglify())
                    .pipe(gulp.dest(path.buildFolder));
                promises.push(promise);
            });
            return plugins.q.all(promises);
        },

        sass: function () {
            /// <summary>
            /// Stylesheet transplier. Most popular one.
            /// </summary>

            var promises = [];
            paths.forEach(function (path) {
                var promise = gulp.src([path.sass, "!" + path.minCss])
                    .pipe(plugins.sass())
                    .pipe(plugins.concat(path.concatCssDest))
                    .pipe(gulp.dest("."));
                promises.push(promise);
            });
            return plugins.q.all(promises);
        },

        less: function () {
            /// <summary>
            /// Alternative to SASS.
            /// </summary>

            var promises = [];
            paths.forEach(function (path) {
                var promise = gulp.src([path.less, "!" + path.minCss])
                    .pipe(plugins.less())
                    .pipe(plugins.concat(path.concatCssDest))
                    .pipe(gulp.dest("."));
                promises.push(promise);
            });
            return plugins.q.all(promises);
        }
    }
};