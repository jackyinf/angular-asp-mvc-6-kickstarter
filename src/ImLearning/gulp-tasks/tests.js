var Server = require('karma').Server;

module.exports = function (gulp, plugins, paths) {
    return {
        karma: function () {
            /// <summary>
            /// Front-end unit tests.
            /// </summary>

            new Server({
// ReSharper disable once UseOfImplicitGlobalInFunctionScope - gulpfile needs this shit.
                configFile: __dirname + '/../karma.conf.js',
                singleRun: false
            }, function done(exitCode) {
                console.log("I'm done. Exit code: " + exitCode);
            }).start();
        }
    }
};