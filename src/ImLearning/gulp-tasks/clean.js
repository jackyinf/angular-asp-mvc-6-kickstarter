﻿module.exports = function(gulp, plugins, paths) {
    return {
        build: function (cb) {
            /// <summary>
            /// Cleans built js code.
            /// </summary>
            /// <param name="cb">Callback. It is needed in order for operation not to freeze.</param>

            return plugins.del(paths[0].buildFolder, cb);
        },

        lib: function (cb) {
            /// <summary>
            /// Cleans library folder (like jquery and other shit).
            /// </summary>
            /// <param name="cb"></param>

            return plugins.del(paths[0].libPathDest, cb);
        }
    }
}