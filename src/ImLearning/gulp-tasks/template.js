﻿module.exports = function(gulp, plugins, paths) {
    return {
        templates: function () {
            /// <summary>
            /// Caches all angular templates into single templates.js file.
            /// </summary>

            var promises = [];
            paths.forEach(function (path) {
                var promise =gulp.src(path.appName + "/**/*.html")
                    .pipe(plugins.angularTemplatecache({
                        module: "templates",
                        moduleSystem: "Browserify",
                        standalone: true
                    }))
                    .pipe(gulp.dest(path.templateDest));
                promises.push(promise);
            });
            return plugins.q.all(promises);
        }
    }
};