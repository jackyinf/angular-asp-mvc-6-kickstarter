﻿module.exports = function(gulp, plugins, paths) {
    return {
        main: function () {
            /// <summary>
            /// Automatic step to take bower_components and copy files to lib folder.
            /// </summary>

            var path = paths[0];
            return gulp.src(plugins.mainBowerFiles())
                .pipe(gulp.dest(path.libPathDest));
        },
        other: function () {
            /// <summary>
            /// Manually include files from bower_components, which are not taken in the automatic step.
            /// </summary>

            var path = paths[0];
            return gulp.src([
                path.bower + "/angular-toastr/dist/angular-toastr.js",
                path.bower + "/angular-toastr/dist/angular-toastr.css",
                path.bower + "/three.js/build/three.min.js"
                    ])
            .pipe(gulp.dest(path.libPathDest));
        }
    }
};