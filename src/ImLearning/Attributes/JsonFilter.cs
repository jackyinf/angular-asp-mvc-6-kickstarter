﻿using System;
using System.IO;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;

namespace ImLearning.Attributes
{
    public class JsonFilter : ActionFilterAttribute
    {
        /// <summary>
        /// Name of the parameter
        /// </summary>
        public string Parameter { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.ContentType.Contains("application/json"))
            {
                string inputContent;
                using (var sr = new StreamReader(filterContext.HttpContext.Request.Body))
                {
                    inputContent = sr.ReadToEnd();
                }

                var result = JsonConvert.DeserializeObject(inputContent, JsonDataType);
                filterContext.ActionArguments[Parameter] = result;
            }
        }
    }
}
