﻿using System;

namespace ImLearning.Models
{
    public class Todo
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTime? Due { get; set; }
    }
}
