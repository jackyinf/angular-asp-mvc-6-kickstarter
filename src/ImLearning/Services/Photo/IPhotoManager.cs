﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using ImLearning.Models;
using Microsoft.AspNet.Http;

namespace ImLearning.Services.Photo
{
    public interface IPhotoManager
    {
        Task<IEnumerable<PhotoViewModel>> Get();
        Task<PhotoActionResult> Delete(string fileName);
        Task<IEnumerable<PhotoViewModel>> Add(HttpRequestMessage request);
        Task<bool> FileExists(string fileName);
        Task<bool> UploadToAzure(IFormFile photo);
    }
}
