﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ImLearning.Services
{
    public interface IBlobService
    {
        bool UploadFile();
        bool ListFiles(out List<dynamic> blobResults);
    }

    public class BlobService : IBlobService
    {
        private const string STORAGE_CONNECTION_STRING = "STORAGE_CONNECTION_STRING";
        private const string CONTAINER = "mycontainer";
        private const string BLOCK_BLOB_REFERENCE = "myblob";

        public bool UploadFile()
        {
            //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("STORAGE_CONNECTION_STRING"));
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(STORAGE_CONNECTION_STRING);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(CONTAINER);
            if (!container.ExistsAsync().Result)
            {
                container.CreateIfNotExistsAsync().Wait();
                container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob }).Wait();
            }

            CloudBlockBlob blockBlob = container.GetBlockBlobReference(BLOCK_BLOB_REFERENCE);

            // Create or overwrite the "myblob" blob with contents from a local file.
            using (var fileStream = File.OpenRead(@"path\myfile"))
            {
                blockBlob.UploadFromStreamAsync(fileStream).Wait();
            }

            return true;
        }

        public bool ListFiles(out List<dynamic> blobResults)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(STORAGE_CONNECTION_STRING);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(CONTAINER);

            // Loop over items within the container and output the length and URI.
            var blobResultSegment = container.ListBlobsSegmentedAsync(null).Result;
            blobResults = new List<dynamic>();
            foreach (IListBlobItem item in blobResultSegment.Results)
            {
                var blockBlob = item as CloudBlockBlob;
                if (blockBlob != null)
                {
                    CloudBlockBlob blob = blockBlob;
                    Console.WriteLine("Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri);
                    blobResults.Add(new { length = blob.Properties.Length, uri = blob.Uri });
                }
                else
                {
                    var blob = item as CloudPageBlob;
                    if (blob != null)
                    {
                        CloudPageBlob pageBlob = blob;
                        Console.WriteLine("Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri);
                        blobResults.Add(new { length = pageBlob.Properties.Length, uri = pageBlob.Uri });
                    }
                    else
                    {
                        var blobDirectory = item as CloudBlobDirectory;
                        if (blobDirectory != null)
                        {
                            CloudBlobDirectory directory = blobDirectory;
                            Console.WriteLine("Directory: {0}", directory.Uri);
                            blobResults.Add(new { length = -1, uri = directory.Uri });
                        }
                    }
                }
            }

            return true;
        }
    }
}
