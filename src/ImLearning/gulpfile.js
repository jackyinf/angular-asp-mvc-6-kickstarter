/// <binding />

/*
 * This task manager manages building several apps. App folders are declared in array "appNames", where names point to
 * the folders in root category.
 */

var gulp = require("gulp");
var gulpLoadPlugins = require("gulp-load-plugins"); // del, concat, cssmin, uglify, sass, templateCache, source, buffer, browserify, babelify, minifyify, mainBowerFiles
var plugins = gulpLoadPlugins({ pattern: ["*"], lazy: true });  // load all of plugins (lazily) which are defined in package.json
var yargs = require('yargs');               // library, which lets you pass arguments for gulp tasks
var project = require("./project");         // project.json - config file for ASP.NET project.
var runSequence = require('run-sequence');  // plugin to run gulp tasks syncroniously

var argv = yargs
    .usage('Usage: gulp.js <gulp-task-name> --<your-app-name>')
    .demand(1)
    .argv;

/*
 *  PATHS
 */
var webroot = "./" + project.webroot + "/";
var appNames = [
    "app",
    "app_es6_playground",
    "app_threejs"
];
if (!argv.all) {
    appNames = appNames.filter(function (name) { return argv[name]; });
}

function getPaths(_appNames) {
    var _paths = [];
    for(var i = 0; i < _appNames.length; i++) {
        var appName = _appNames[i];
        console.log("Using app \"" + appName + "\".");

        _paths.push({
            webroot: webroot,
            appName: appName,
            bower: project.bower,

            js: "./" + appName + "/**/*.js",
            minJs: "./" + appName + "/**/*.min.js",
            sass: "./" + appName + "/**/*.scss",
            less: "./" + appName + "/**/*.less",
            minCss: "./" + appName + "/**/*.min.css",
            templateDest: "./" + appName,

            libPathDest: webroot + appName + "/lib",
            buildFolder: webroot + "build/",
            concatJsDest: webroot + "build/" + appName + ".js",
            concatCssDest: webroot + "build/" + appName + ".css",
            concatMinJsDest: webroot + "build/" + appName + ".min.js",
            concatMinCssDest: webroot + "build/" + appName + ".min.css"
        })
    }
    return _paths;
}

var paths = getPaths(appNames);

/*
 *   SUBTASKS
 */
var cleanTasks = require("./gulp-tasks/clean")(gulp, plugins, paths);
gulp.task("clean:build", cleanTasks.build);
gulp.task("clean:lib", cleanTasks.lib);

var bowerTasks = require("./gulp-tasks/bower")(gulp, plugins, paths);
gulp.task("bower-copy:other", bowerTasks.other);
gulp.task("bower-copy:main", bowerTasks.main);
gulp.task("bower-copy", ["bower-copy:other", "bower-copy:main"]);

var templateTasks = require("./gulp-tasks/template")(gulp, plugins, paths);
gulp.task("templates", templateTasks.templates);

var buildTasks = require("./gulp-tasks/build")(gulp, plugins, paths);
gulp.task("build:js", ["templates"], buildTasks.js);
gulp.task("build:js:deploy", ["templates"], buildTasks.jsDeploy);
gulp.task("build:css", ["templates"], buildTasks.less);
gulp.task("build", function () {
    runSequence("clean:build", "build:js", "build:css", function () {
        console.log("Build task completed.")
    })
});

/*
 *  MAIN TASKS
 */
gulp.task("default", ["build", "bower-copy", "watch"]);
function watcher(appName) {
    gulp.watch([appName + "/**/*.js", "!" + appName + "/**/*.spec.js", "!" + appName + "/templates.js", appName + "/**/*.html", appName + "/**/*.less"], ["build"]);
}
gulp.task("watch", ["build"], function() { watcher(appNames[0]); });
gulp.task("deploy", ["clean", "build:js:deploy", "build:css"]);

/*
 *  FRONTEND TEST TASKS
 */
var testTasks = require("./gulp-tasks/tests")(gulp, plugins, paths);
gulp.task("test", testTasks.karma);
gulp.task("dummy", function () {});