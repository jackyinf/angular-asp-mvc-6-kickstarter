/**
 * Created by zekar on 10/18/2015.
 */
const INIT = new WeakMap();

class ThreejsMainController
{
    constructor() {
        var vm = this;
        vm.title = "Threejs main page";
        INIT.set(this, () => {
            console.log("INIT called from Threejs main controller");
        });
        this.activate();
    }

    activate() {
        INIT.get(this)();
    }
}

export default ThreejsMainController;