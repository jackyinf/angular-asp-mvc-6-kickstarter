/**
 * Created by zekar on 10/18/2015.
 */
function config($stateProvider) {
    $stateProvider
        .state("threejs", {
            url: '/',
            views: {
                nav: { },
                content: {
                    templateUrl: "threejs/threejs.main.html",
                    controller: "threejs.main.controller",
                    controllerAs: "vm"
                }
            }
        })
}

config.$inject = ["$stateProvider"];

export default config;