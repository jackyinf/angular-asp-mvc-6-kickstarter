/**
 * Created by zekar on 10/18/2015.
 */

// Angular modules
import angular from "angular";
import uiRouter from "angular-ui-router";

// Custom modules
import config from "./threejs.main.config.js";
import threejsMainController from "./threejs.main.controller";

const moduleName = "threejs";

angular
    .module(moduleName, [uiRouter])
    .controller("threejs.main.controller", threejsMainController)
    .config(config);

export default moduleName;