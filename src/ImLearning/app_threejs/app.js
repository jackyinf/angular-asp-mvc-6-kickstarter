/**
 * Created by zekar on 10/18/2015.
 */
import "babelify/polyfill";

// Angular modules
import angular from "angular";
import uiRouter from "angular-ui-router";
import angularResource from "angular-resource";

// Custom modules
import templates from "./templates";
import config from "./app.config";
import threejsModule from "./threejs/threejs.module";

angular
    .module("app", [uiRouter, templates.name, threejsModule])
    .config(config);