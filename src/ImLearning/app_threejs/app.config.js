/**
 * Created by zekar on 10/18/2015.
 */
function config($stateProvider) {
    $stateProvider
        .state("app", {
            url: '/',
            views: {
                nav: { },
                content: {
                    template: "App"
                }
            }
        })
        .state("otherwise", {
            url: "*path",
            redirectTo: 'app'
        });
}

export default config;