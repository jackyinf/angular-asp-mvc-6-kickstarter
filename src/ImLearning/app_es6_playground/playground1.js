﻿export default (function() {
    console.log("playground 1");

    // Symbols
    var blabla = ['a', 'b', 'c', 'd', 'e'];
    var blablaIt = blabla[Symbol.iterator]();
    console.log(blablaIt.next());
    var aaa = Symbol("test");
    console.log(aaa.value);
    var a = Symbol.for("test");
    console.log(a.value);
    var b = Symbol.for("test");
    console.log(a === b);
    console.log(a === aaa);

    // Iterators
    var arr = [1, 2, 3, 4, 5];
    for (var val of arr) {
        console.log(val);
    }

    //
    //  FIBONACCI
    //
    console.log(`Fibonacci`);
    var Fib = {
        [Symbol.iterator]() {
            var n1 = 1, n2 = 1;

            return {
                [Symbol.iterator]() {
                    return this;
                },

                next() {
                    var current = n2;
                    n2 = n1;
                    n1 = n1 + current;
                    return { value: current, done: false }
                },

                return (v) {
                    console.log("Fibonacci stopped.");
                    return { value: v, done: true };
                }
            }
        }
    }
    for (var fibVal of Fib) {
        console.log(fibVal);

        if (fibVal > 50)
            break;
    }

    //
    //  TASKS
    //
    console.log(`Tasks`);
    var tasks = {
        actions: [],

        [Symbol.iterator]() {
            var steps = this.actions.slice();

            return {
                [Symbol.iterator]() {
                    return this;
                },

                next(...args) {
                    if (steps.length > 0) {
                        let res = steps.shift()(...args);
                        return { value: res, done: false };
                    } else {
                        return { done: true }
                    }
                },

                return(v) {
                    steps.length = 0;
                    return { value: v, done: true };
                }
            }
        }
    };

    tasks.actions.push(
        function step1(x) {
            console.log(`Step 1: ${x}`);
            return x * 2;
        }
    );

    var it = tasks[Symbol.iterator]();

    it.next(10);
    it.next();

    //
    //  NUMBER ITERATOR
    //

    if (!Number.prototype[Symbol.iterator]) {
        Object.defineProperty(
            Number.prototype,
            Symbol.iterator,
            {
                writable: true,
                configurable: true,
                enumerable: false,
                value: function iterator() {
                    var i, inc, done = false, top = +this;

                    inc = 1 * (top < 0 ? -1 : 1);

                    return {
                        [Symbol.iterator]() {
                            return this;
                        },

                        next() {
                            if (done) {
                                return { done: true };
                            }

                            if (i == null) {
                                i = 0;
                            } else if (top >= 0) {
                                i = Math.min(top, i + inc);
                            } else {
                                i = Math.max(top, i + inc);
                            }

                            if (i == top)
                                done = true;

                            return { value: i, done: false };
                        }
                    }
                }
            }
        );
    }

    console.log(`Number iterator`);
    for (var i of 3) {
        console.log(i);
    }
})();