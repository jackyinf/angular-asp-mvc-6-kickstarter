﻿console.log("playground 2! ");

// Generators
console.log("Test 1");
function *foo() {
    yield 1;
    yield 2;
    yield 3;
}

var it = foo();
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());

console.log("Test 2");
function *bar(x) {
    if (x < 3) {
        x = yield *bar(x + 1);
    }
    return x * 2;
}

var barIt = bar(1);
console.log(barIt.next());