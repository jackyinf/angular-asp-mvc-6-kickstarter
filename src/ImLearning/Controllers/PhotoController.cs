﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using ImLearning.Services.Photo;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.WebApiCompatShim;

namespace ImLearning.Controllers
{
    public class PhotoController : Controller
    {
        private readonly IPhotoManager photoManager;
        
        public PhotoController(IPhotoManager photoManager)
        {
            this.photoManager = photoManager;
        }

        // GET: api/Photo
        [Route("api/photo")]
        public async Task<IActionResult> Get()
        {
            var results = await photoManager.Get();
            return Ok(new { photos = results });
        }

        // POST: api/Photo
        [Route("api/photo"), HttpPost]
        public async Task<IActionResult> Post()
        {
            // Check if the request contains multipart/form-data.
            var msg = Context.GetHttpRequestMessage();
            if (!msg.Content.IsMimeMultipartContent("form-data"))
            {
                Response.StatusCode = 500;
                return new InternalServerErrorResult();
            }

            try
            {
                var photos = await photoManager.Add(msg);
                return Ok(new { Successful = true, Message = "Photos uploaded ok", Photos = photos });
            }
            catch (Exception ex)
            {
                return Ok(new { Successful = false, Message = ex.Message });
            }
        }

        // DELETE: api/Photo/5
        [HttpDelete]
        [Route("api/Photo/{fileName}")]
        public async Task<IActionResult> Delete(string fileName)
        {
            if (!await this.photoManager.FileExists(fileName))
            {
                return Ok(new { Successful = false, Message = "File not found" });
                //return NotFound();
            }

            var result = await this.photoManager.Delete(fileName);

            if (result.Successful)
            {
                return Ok(result.Message);
            }
            else
            {
                return Ok(new { Successful = false, Message = result.Message });
                //return BadRequest(result.Message);
            }
        }

        // TODO: This is experiment.
        public IActionResult Index()
        {
            return View();
        }

        // TODO: This is experiment.
        public IActionResult Upload(IFormFile photo)
        {
            dynamic fileDetails = null;
            using (var reader = new StreamReader(photo.OpenReadStream()))
            {
                var fileContent = reader.ReadToEnd();
                var parsedContentDisposition = ContentDispositionHeaderValue.Parse(photo.ContentDisposition);
                fileDetails = new
                {
                    Filename = parsedContentDisposition.FileName,
                    Content = fileContent
                };
            }

            return RedirectToAction("Index");
        }

        // TODO: This is experiment.
        public async Task<IActionResult> UploadToAzure(IFormFile photo)
        {
            await photoManager.UploadToAzure(photo);
            return RedirectToAction("Index");
        }

        //// http://stackoverflow.com/questions/12494067/read-httpcontent-in-mvc-webapi-controller
        //[HttpPut]
        //public HttpResponseMessage Put(int accountId)
        //{
        //    HttpContent requestContent = Request.Content;
        //    string jsonContent = requestContent.ReadAsStringAsync().Result;
        //    CONTACT contact = JsonConvert.DeserializeObject<CONTACT>(jsonContent);
        //    ...
        //}
    }
}
