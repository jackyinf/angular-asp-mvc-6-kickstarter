﻿using System;
using ImLearning.Attributes;
using ImLearning.Models;
using Microsoft.AspNet.Mvc;

namespace ImLearning.Controllers
{
    public class TodoController : Controller
    {
        [JsonFilter(Parameter = "todo", JsonDataType = typeof(Todo))]
        public JsonResult Add(Todo todo)
        {
            var response = Json(new {success = todo.Id == Guid.Empty});
            response.StatusCode = todo.Id == Guid.Empty ? 500 : 200;
            return response;
        }
    }
}
