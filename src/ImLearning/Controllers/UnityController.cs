﻿using System.IO;
using Microsoft.AspNet.Mvc;

namespace ImLearning.Controllers
{
    public class UnityController : Controller
    {
        public IActionResult Supernova()
        {
            return View();
        }

        public FilePathResult SupernovaDataFile()
        {
            var path = Path.Combine("~/unity_content/", "build05_Web.unity3d");
            return base.File(path, "application/vnd.unity");
        }

        public IActionResult Exhibition()
        {
            return View();
        }

        public FilePathResult ExhibitionDataFile()
        {
            var path = Path.Combine("~/unity_content/", "exhibition_v0.1.unity3d");
            return base.File(path, "application/vnd.unity");
        }
        
    }
}
