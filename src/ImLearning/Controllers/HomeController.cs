using Microsoft.AspNet.Mvc;

namespace ImLearning.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult App()
        {
            return View();
        }

        public IActionResult Es6Playground()
        {
            return View();
        }

        public IActionResult Threejs()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }
    }
}
