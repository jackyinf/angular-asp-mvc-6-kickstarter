# Welcome to my ASP.NET 5 starter app

## About ##

The purpose of this project is to learn to use best practices for developing robust web applications.

Frontend technologies used:
* Angular
* Gulp
* NPM
* Bower
* ES6
* Karma + Jasmine
* ...

Backend technologies used:
* Entity framework 7 with Authentication
* ...

## Getting started ##

Install dependencies

```
#!bash

npm install
bower install
```

Run with gulp

```
#!bash

gulp
```

Run unit tests

```
#!bash

gulp test
```